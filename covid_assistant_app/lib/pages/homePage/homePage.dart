import 'dart:ui';
// import 'package:covid_assistant_app/widgets/cardAnggota/DetailScreen.dart';

import 'package:covid_assistant_app/constants/colors.dart';
import 'package:covid_assistant_app/widgets/cardAnggota/Data.dart';
import 'package:covid_assistant_app/widgets/cardFitur/dataFitur.dart';
import 'package:flutter/material.dart';
import 'package:covid_assistant_app/Screen/drawer/side_bar_drawer.dart';
import 'package:covid_assistant_app/pages/landingPage/LandingPageHome.dart';
import 'package:covid_assistant_app/widgets/formFeedback/formFeedback.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
// import 'package:curved_navigation_bar/curved_navigation_bar.dart';

class MyHomePage extends StatelessWidget {
  static const routeName = '/home';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(6, 12, 35, 1),
        centerTitle: true,
        title: Image.asset(
          "assets/images/logo.png",
          width: 180,
        ),
      ),
      extendBody: true,
      drawer: DrawerScreen(),
      body: Container(
        decoration: BoxDecoration(
          color: Color.fromRGBO(6, 12, 35, 1),
        ),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              // Navbar(),
              Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 20.0, horizontal: 40.0),
                child: LandingPageHome(),
              ),
              Container(
                decoration: BoxDecoration(
                  color: Color.fromRGBO(232, 247, 252, 1),
                ),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 100,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 20.0, horizontal: 40.0),
                      child: Image.asset(
                        "assets/images/About.png",
                        width: 420,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      "ABOUT US",
                      style: TextStyle(
                        color: Color.fromRGBO(23, 194, 236, 1),
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Apa itu Covid Assistant ?",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 26.0,
                          color: Colors.black),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 20.0, horizontal: 40),
                      child: Text(
                        "Covid Assistant (CoAss) yang merupakan aplikasi reminder dan tracking untuk pasien yang terpapar Covid-19. Tujuan dari aplikasi ini sama seperti namanya yaitu asisten untuk para pejuang Covid-19 dan membantu mereka dalam progress penyembuhan.",
                        style: TextStyle(
                            fontSize: 16.0,
                            color: AppColors.textColor1,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                    SizedBox(
                      height: 100,
                    ),
                    Text(
                      "Covid Assistant",
                      style: TextStyle(
                        color: Color.fromRGBO(23, 194, 236, 1),
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        fontFamily: "Poppins",
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Tim Kami",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      height: 550,
                      padding: const EdgeInsets.only(left: 32),
                      child: Swiper(
                        itemCount: namaAnggota.length,
                        itemWidth: MediaQuery.of(context).size.width - 2 * 64,
                        layout: SwiperLayout.STACK,
                        itemBuilder: (context, index) {
                          return InkWell(
                            onTap: () {},
                            child: Stack(
                              children: <Widget>[
                                Column(
                                  children: <Widget>[
                                    SizedBox(height: 100),
                                    Card(
                                      elevation: 8,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(32),
                                      ),
                                      color: Colors.white,
                                      child: Padding(
                                        padding: const EdgeInsets.all(32.0),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            SizedBox(height: 100),
                                            Text(
                                              namaAnggota[index].name,
                                              style: TextStyle(
                                                  fontFamily: 'Poppins',
                                                  fontSize: 24,
                                                  color:
                                                      const Color(0xff47455f),
                                                  fontWeight: FontWeight.w800),
                                              textAlign: TextAlign.left,
                                            ),
                                            Text(
                                              'Sistem Informasi 2020',
                                              style: TextStyle(
                                                fontFamily: 'Poppins',
                                                fontSize: 18,
                                                color: AppColors.textColor1,
                                                fontWeight: FontWeight.w500,
                                              ),
                                              textAlign: TextAlign.left,
                                            ),
                                            SizedBox(height: 32),
                                            Row(
                                              children: <Widget>[
                                                Text(
                                                  'Know more',
                                                  style: TextStyle(
                                                    fontFamily: 'Poppins',
                                                    fontSize: 16,
                                                    color:
                                                        AppColors.mainTextColor,
                                                    fontWeight: FontWeight.w500,
                                                  ),
                                                  textAlign: TextAlign.left,
                                                ),
                                                Icon(
                                                  Icons.arrow_forward,
                                                  color:
                                                      AppColors.mainTextColor,
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Hero(
                                  tag: namaAnggota[index].name,
                                  child: Image.asset(
                                    namaAnggota[index].imageUrl,
                                  ),
                                ),
                                Positioned(
                                  right: 24,
                                  bottom: 80,
                                  child: Text(
                                    namaAnggota[index].position.toString(),
                                    style: TextStyle(
                                      fontFamily: 'Avenir',
                                      fontSize: 200,
                                      color: AppColors.textColor1
                                          .withOpacity(0.08),
                                      fontWeight: FontWeight.w900,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                    ),
                    SizedBox(
                      height: 100,
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 20, right: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Fitur Kami",
                            style: TextStyle(
                              color: AppColors.titleTextColor,
                              fontSize: 18,
                              fontWeight: FontWeight.w600,
                              fontFamily: "Poppins",
                            ),
                          ),
                          Text(
                            "Lihat semua",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: AppColors.textColor1,
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                      padding: const EdgeInsets.only(bottom: 12),
                    ),
                    Column(
                      children: [
                        SizedBox(
                          height: 300,
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: 6,
                            itemBuilder: (BuildContext context, int index) {
                              return InkWell(
                                onTap: () {
                                  Navigator.of(context)
                                      .pushNamed(fitur[index].route);
                                },
                                child: Stack(
                                  children: <Widget>[
                                    Container(
                                        margin: const EdgeInsets.only(
                                            left: 15, right: 10),
                                        width: 200,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(20),
                                          color: Colors.white,
                                          boxShadow: [
                                            BoxShadow(
                                                color: Colors.grey
                                                    .withOpacity(0.3),
                                                spreadRadius: 0.3,
                                                blurRadius: 5.0)
                                          ],
                                        ),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(4.0),
                                              child: Hero(
                                                  tag: fitur[index].namaFitur,
                                                  child: Container(
                                                    width: 200,
                                                    height: 200,
                                                    decoration: BoxDecoration(
                                                        image: DecorationImage(
                                                            image: AssetImage(
                                                                fitur[index]
                                                                    .imageUrl),
                                                            fit: BoxFit
                                                                .contain)),
                                                  )),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 12, top: 4),
                                              child: Text(
                                                fitur[index].namaFitur,
                                                style: TextStyle(
                                                  color:
                                                      const Color(0xff47455f),
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w800,
                                                  fontFamily: "Poppins",
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 12, top: 16),
                                              child: Row(
                                                children: [
                                                  Text(
                                                    'Cek disini',
                                                    style: TextStyle(
                                                      fontFamily: 'Poppins',
                                                      fontSize: 16,
                                                      color: AppColors
                                                          .mainTextColor,
                                                      fontWeight:
                                                          FontWeight.w500,
                                                    ),
                                                    textAlign: TextAlign.left,
                                                  ),
                                                  Icon(
                                                    Icons.arrow_forward,
                                                    color:
                                                        AppColors.mainTextColor,
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        )),
                                  ],
                                ),
                              );
                            },
                          ),
                        ),
                        SizedBox(
                          height: 100,
                        )
                      ],
                    ),
                    Text(
                      "FEEDBACK",
                      style: TextStyle(
                        color: Color.fromRGBO(23, 194, 236, 1),
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        fontFamily: "Poppins",
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Tulis Feedback Anda Tentang Kami",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 50.0, horizontal: 40.0),
                      child: FormFeedback(),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
