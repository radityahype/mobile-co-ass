import 'package:flutter/material.dart';

class LandingPageFeedback extends StatelessWidget {
  List<Widget> pageChildren(double width) {
    return <Widget>[
      Container(
        decoration: BoxDecoration(
          color: Color.fromRGBO(6, 12, 35, 1),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 30,
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
              child: Image.asset(
                "assets/images/Feedback.png",
                width: 420,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              "COVID ASSISTANT",
              style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 20.0,
                  color: Color.fromRGBO(23, 194, 236, 1)),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              "Feedback",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 50.0,
                  color: Colors.white),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20.0),
              child: Text(
                "Terima kasih telah memberikan feedback kepada kami. Hal ini sangat membantu perbaikan untuk web kami kedepannya. Salam kelompok B03",
                style: TextStyle(fontSize: 16.0, color: Colors.white),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            MaterialButton(
              color: Color.fromRGBO(6, 12, 35, 1),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(40.0)),
                  side: BorderSide(color: Color.fromRGBO(23, 194, 236, 1))),
              onPressed: () {},
              child: Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 12.0, horizontal: 16.0),
                child: Text(
                  "Lihat feedback",
                  style: TextStyle(
                      color: Color.fromRGBO(23, 194, 236, 1), fontSize: 14.0),
                ),
              ),
            ),
            SizedBox(
              height: 24,
            ),
          ],
        ),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth > 800) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: pageChildren(constraints.biggest.width / 2),
          );
        } else {
          return Column(
            children: pageChildren(constraints.biggest.width),
          );
        }
      },
    );
  }
}
