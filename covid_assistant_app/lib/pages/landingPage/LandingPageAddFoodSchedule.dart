import 'package:covid_assistant_app/pages/FoodSchedulePage/AddFoodSchedulePage.dart';
import 'package:flutter/material.dart';

class LandingPageFoodSchedule extends StatelessWidget {
  List<Widget> pageChildren(double width) {
    return <Widget>[
      Container(
        width: width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 30,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20.0),
              child: Image.asset(
                "assets/images/fitur-3.png",
                width: 420,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              "COVID ASSISTANT",
              style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 20.0,
                  color: Color.fromRGBO(23, 194, 236, 1)),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              "Jadwal Makan Minum",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 50.0,
                  color: Colors.white),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20.0),
              child: Text(
                "Food & Drink Schedule atau Jadwal Makan dan Minum merupakan salah satu fitur yang dimiliki oleh Covid Assistant. Dengan adanya fitur ini Anda dapat mencatat makanan dan minum apa yang telah dikonsumsi anda. Sebuah pasien covid dapat kehilangan indera pengecap dan atau indera penciuman sehingga pasien dapat lupa makan dan minum , Pasien dapat memanfaatkan fitur ini untuk mencatat serta melihat apa yang sudah dikonsumsi mereka. Fitur ini menyediakan formulir untuk pengguna untuk mencatat konsumsi makan dan minum serta pengguna dapat melihat langsung ringkasan makan dan minum perhari atau secara keseluruhan.",
                style: TextStyle(fontSize: 16.0, color: Colors.white),
              ),
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth > 800) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: pageChildren(constraints.biggest.width / 2),
          );
        } else {
          return Column(
            children: pageChildren(constraints.biggest.width),
          );
        }
      },
    );
  }
}
