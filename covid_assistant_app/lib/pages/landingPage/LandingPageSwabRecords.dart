import 'package:covid_assistant_app/pages/DailyJournalPage/AddDailyJournalPage.dart';
import 'package:flutter/material.dart';

class LandingPageSwabRecords extends StatelessWidget {
  List<Widget> pageChildren(double width) {
    return <Widget>[
      Container(
        width: width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 30,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20.0),
              child: Image.asset(
                "assets/images/fitur-5.png",
                width: 420,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              "COVID ASSISTANT",
              style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 20.0,
                  color: Color.fromRGBO(23, 194, 236, 1)),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              "Swab Records",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 50.0,
                  color: Colors.white),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20.0),
              child: Text(
                "Swab Records adalah fitur yang disediakan oleh Covid Assistant untuk membantu anda melakukan pencatatan terhadap data dari tes swab yang pernah anda lakukan. Data-data yang disimpan adalah tanggal anda melakukan tes swab, hasil, serta CT Value.",
                style: TextStyle(fontSize: 16.0, color: Colors.white),
              ),
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth > 800) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: pageChildren(constraints.biggest.width / 2),
          );
        } else {
          return Column(
            children: pageChildren(constraints.biggest.width),
          );
        }
      },
    );
  }
}
