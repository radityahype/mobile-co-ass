import 'package:covid_assistant_app/pages/DailyJournalPage/AddDailyJournalPage.dart';
import 'package:flutter/material.dart';

class LandingPageHealthRecords extends StatelessWidget {
  List<Widget> pageChildren(double width) {
    return <Widget>[
      Container(
        width: width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 30,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20.0),
              child: Image.asset(
                "assets/images/fitur-4.png",
                width: 420,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              "COVID ASSISTANT",
              style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 20.0,
                  color: Color.fromRGBO(23, 194, 236, 1)),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              "Health Records",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 50.0,
                  color: Colors.white),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20.0),
              child: Text(
                "Health Records adalah salah satu fitur yang dimiliki oleh Covid Assistant. Dengan adanya fitur ini Anda dapat mencatat komponen kesehatan. Ketika pasien covid sedang isolasi mandiri, mereka juga harus memperhatikan kesehatan mereka dengan memeriksa beberapa komponen kesehatan, seperti suhu, tensi, dan saturasi. Mereka bisa memanfaatkan fitur ini untuk mencatat serta tracking kesehatan mereka. Fitur ini menyediakan form untuk user mengisi catatan kesehatan mereka, serta user juga bisa langsung melihat ringkasan catatan kesehatannya.",
                style: TextStyle(fontSize: 16.0, color: Colors.white),
              ),
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth > 800) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: pageChildren(constraints.biggest.width / 2),
          );
        } else {
          return Column(
            children: pageChildren(constraints.biggest.width),
          );
        }
      },
    );
  }
}
