import 'dart:convert';
// import 'dart:js';

import 'package:covid_assistant_app/api/models/DailyJournalModel.dart';
import 'package:covid_assistant_app/pages/landingPage/LandingPageDailyJournal.dart';
import 'package:covid_assistant_app/widgets/card/cardFeedback.dart';
import 'package:covid_assistant_app/widgets/cardDailyJournal/cardDailyjournal.dart';
import 'package:flutter/material.dart';
import 'package:covid_assistant_app/Screen/drawer/side_bar_drawer.dart';
import 'package:provider/src/provider.dart';

import '../../cookies.dart';
import 'AddDailyJournalPage.dart';

import 'package:http/http.dart' as http;


class DailyJournalPage extends StatefulWidget {
  static const routeName = '/daily-journal';

  const DailyJournalPage({ Key? key }) : super(key: key);

  @override
  _DailyJournalPageState createState() => _DailyJournalPageState();
}

class _DailyJournalPageState extends State<DailyJournalPage> {
  bool isUser = false;

List<DailyJournalMessage> dailyJournalUser = [];

Future getUserDataDailyJournal(BuildContext context) async {
  try{
   dailyJournalUser = [];
   final request = context.watch<CookieRequest>();
  //  final response = await request.get(url);
   final response = await request.get("https://co-ass.herokuapp.com/daily-journal/card-flutter");
   print(response);

    for(var i in response){
      Fields fields = Fields(
        hari:  i["fields"]["hari"],
        tanggal: i["fields"]["tanggal"],
        isDemam:  i["fields"]["is_demam"],
        isBatuk:  i["fields"]["is_batuk"],
        isKelelahan:  i["fields"]["is_kelelahan"],
        isPenciuman:  i["fields"]["is_penciuman"],
        curhat:  i["fields"]["curhat"],
        user:  i["fields"]["user"],
      );
      DailyJournalMessage feedback =
      DailyJournalMessage(model: i["model"], pk: i["pk"], fields: fields);
      dailyJournalUser.add(feedback);
    }
    print(dailyJournalUser);
    return dailyJournalUser;
  } catch(error) {
    print("error");
    print(error);
    print(dailyJournalUser);
  }
}
  @override
  Widget build(BuildContext context) {
    final request = context.watch<CookieRequest>();
   request.username != "" ?isUser = true : isUser = false;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(6, 12, 35, 1),
        centerTitle: true,
        title: Text("Daily Journal"),
      ),
      extendBody: true,
      drawer: DrawerScreen(),
      body: Container(
        decoration: BoxDecoration(
          color: Color.fromRGBO(6, 12, 35, 1),
        ),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              // Navbar(),
              Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 20.0, horizontal: 20),
                child: LandingPageDailyJournal(),
              ),
              MaterialButton(
              color: Color.fromRGBO(6, 12, 35, 1),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(40.0)),
                  side: BorderSide(color: Color.fromRGBO(23, 194, 236, 1))),
              onPressed: () {
                 Navigator.of(context).pushNamed(AddDailyJournalPage.routeName);
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 12.0, horizontal: 16.0),
                child: Text(
                  "Tambah Jurnal",
                  style: TextStyle(
                      color: Color.fromRGBO(23, 194, 236, 1), fontSize: 14.0),
                ),
              ),
            ),
            SizedBox(
              height: 50,
            ),
              Container(
                decoration: BoxDecoration(
                  color: Color.fromRGBO(232, 247, 252, 1),
                ),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 100,
                    ),
                    Text(
                      "Daily Journal",
                      style: TextStyle(
                        color: Color.fromRGBO(23, 194, 236, 1),
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                   SingleChildScrollView(
                        child: FutureBuilder(
                          future: getUserDataDailyJournal(context),
                          builder: (context, AsyncSnapshot snapshot) {
                            if (snapshot.data == null) {
                              return Container(
                                child: Center(
                                    child: Text(
                                      "Loading...",
                                    )),
                              );
                            } else {
                              return Column(
                                  children:
                                  dailyJournalUser.map((data) {
                                    return DailyJournalCard(
                                      hari: data.fields.hari,
                                      tanggal: data.fields.tanggal.toString(),
                                      demam: data.fields.isDemam.toString(),
                                      batuk: data.fields.isBatuk.toString(),
                                      kelelahan: data.fields.isKelelahan.toString(),
                                      kehilanganPenciuman: data.fields.isPenciuman.toString(),
                                      keluhan: data.fields.curhat,
                                    );
                                  }).toList()
                              );
                      
                            }
                                      }),
                      ),
                    SizedBox(height: 120),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).pop();
        },
        backgroundColor: Colors.cyan,
        child: Icon(Icons.keyboard_arrow_left),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      // bottomNavigationBar: CurvedNavigationBar(
      //   color: Colors.cyan,
      //   backgroundColor: Colors.transparent,
      //   items: items,
      //   index: index,
      //   height: 50,
      // ),
    );
  }
}
