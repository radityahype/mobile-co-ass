import 'dart:convert';

import 'package:covid_assistant_app/constants/colors.dart';
import 'package:covid_assistant_app/pages/DailyJournalPage/DailyJournalPage.dart';
import 'package:covid_assistant_app/pages/landingPage/LandingPageAddDailyJournal.dart';
import 'package:covid_assistant_app/widgets/card/cardFeedback.dart';
import 'package:covid_assistant_app/widgets/formDailyJournal/formDailyJournal.dart';
import 'package:flutter/material.dart';
import 'package:covid_assistant_app/Screen/drawer/side_bar_drawer.dart';
import 'package:covid_assistant_app/pages/landingPage/LandingPageFeedback.dart';

import 'package:http/http.dart' as http;

// import 'package:curved_navigation_bar/curved_navigation_bar.dart';

class AddDailyJournalPage extends StatelessWidget {
  static const routeName = '/tambah-jurnal';
  static const url = "http://10.0.2.2:8000/json_function";
  const AddDailyJournalPage({Key? key}) : super(key: key);
  Future<List<dynamic>> _fecthDataUsers() async {
    var result = await http.get(Uri.parse(url));
    print(result);
    print("ini body");
    print(result.body);
    print(jsonDecode(result.body)["data"]);
    return json.decode(result.body)['data'];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(6, 12, 35, 1),
        centerTitle: true,
        title: Text("Add Daily Journal"),
      ),
      extendBody: true,
      drawer: DrawerScreen(),
      body: Container(
        decoration: BoxDecoration(
          color: AppColors.backgroundColor,
        ),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              // Navbar(),
              Container(
              decoration: BoxDecoration(color: Color.fromRGBO(6, 12, 35, 1)),
              child: 
              Column(
                children: [
                  LandingPageAddDailyJournal(),
                   MaterialButton(
              color: Color.fromRGBO(6, 12, 35, 1),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(40.0)),
                  side: BorderSide(color: Color.fromRGBO(23, 194, 236, 1))),
              onPressed: () {
                 Navigator.of(context).pushNamed(DailyJournalPage.routeName);
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 12.0, horizontal: 16.0),
                child: Text(
                  "Lihat Jurnal",
                  style: TextStyle(
                      color: Color.fromRGBO(23, 194, 236, 1), fontSize: 14.0),
                ),
              ),
            ),
            SizedBox(
              height: 100,
            ),
                ],
              ) ),
             Container(             
               child: Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 40.0, horizontal: 40.0),
                        child: FormDailyJournal(),
                      ),
             ),
              SizedBox(
                height:100
              ),
              // Container(
              //   // width = MediaQuery.of(context).size.width,
              //   decoration: BoxDecoration(
              //     color: Color.fromRGBO(232, 247, 252, 1),
              //   ),
              //   child: Column(
              //     children: <Widget>[
              //       SizedBox(
              //         height: 100,
              //       ),
              //       Text(
              //         "FEEDBACK",
              //         style: TextStyle(
              //           color: Color.fromRGBO(23, 194, 236, 1),
              //           fontSize: 20,
              //           fontWeight: FontWeight.bold,
              //         ),
              //       ),
              //       SizedBox(height: 120),
              //     ],
              //   ),
              
              // ),
              
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).pop();
        },
        backgroundColor: Colors.cyan,
        child: Icon(Icons.keyboard_arrow_left),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }
}
