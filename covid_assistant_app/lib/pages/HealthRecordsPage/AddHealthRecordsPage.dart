import 'dart:convert';

import 'package:covid_assistant_app/pages/landingPage/LandingPageAddHealthRecords.dart';
import 'package:covid_assistant_app/widgets/card/cardFeedback.dart';
import 'package:covid_assistant_app/widgets/formHealthRecords/formHealthRecords.dart';
import 'package:flutter/material.dart';
import 'package:covid_assistant_app/Screen/drawer/side_bar_drawer.dart';

import 'package:http/http.dart' as http;

// import 'package:curved_navigation_bar/curved_navigation_bar.dart';

class AddHealthRecordsPage extends StatelessWidget {
  static const routeName = '/tambah-catatanKesehatan';
  static const url = "http://10.0.2.2:8000/json_function";
  const AddHealthRecordsPage({Key? key}) : super(key: key);
  Future<List<dynamic>> _fecthDataUsers() async {
    var result = await http.get(Uri.parse(url));
    print(result);
    print("ini body");
    print(result.body);
    print(jsonDecode(result.body)["data"]);
    return json.decode(result.body)['data'];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(6, 12, 35, 1),
        centerTitle: true,
        title: Text("Feedback"),
      ),
      extendBody: true,
      drawer: DrawerScreen(),
      body: Container(
        decoration: BoxDecoration(
          color: Color.fromRGBO(6, 12, 35, 1),
        ),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 20.0, horizontal: 40.0),
                child: LandingPageAddHealthRecords(),
              ),
              Container(
                // width = MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  color: Color.fromRGBO(232, 247, 252, 1),
                ),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 50.0, horizontal: 40.0),
                      child: FormHealthRecords(),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              
              ),
              
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).pop();
        },
        backgroundColor: Colors.cyan,
        child: Icon(Icons.keyboard_arrow_left),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }
}
