import 'package:covid_assistant_app/constants/colors.dart';

import 'package:covid_assistant_app/pages/HealthRecordsPage/AddHealthRecordsPage.dart';

import 'package:covid_assistant_app/pages/landingPage/LandingPageHealthRecords.dart';
import 'package:flutter/material.dart';

import 'AddHealthRecordsPage.dart';
import 'package:covid_assistant_app/Screen/drawer/side_bar_drawer.dart';
import 'package:covid_assistant_app/widgets/cardInformasiKesehatan/infoKesehatan.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'dart:ui';

import 'package:covid_assistant_app/widgets/cardHealthRecords/cardHealthRecords.dart';

class HealthRecordsPage extends StatelessWidget {
  static const routeName = "/health-records";
  const HealthRecordsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(6, 12, 35, 1),
        centerTitle: true,
        title: Text("Health Records"),
      ),
      extendBody: true,
      drawer: DrawerScreen(),
      body: Container(
        decoration: BoxDecoration(
          color: Color.fromRGBO(6, 12, 35, 1),
        ),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              // Navbar(),
              Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 20.0, horizontal: 20),
                child: LandingPageHealthRecords(),
              ),
              MaterialButton(
              color: Color.fromRGBO(6, 12, 35, 1),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(40.0)),
                  side: BorderSide(color: Color.fromRGBO(23, 194, 236, 1))),
              onPressed: () {
                 Navigator.of(context).pushNamed(AddHealthRecordsPage.routeName);
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 12.0, horizontal: 16.0),
                child: Text(
                  "Tambah Catatan Kesehatan",
                  style: TextStyle(
                      color: Color.fromRGBO(23, 194, 236, 1), fontSize: 14.0),
                ),
              ),
            ),
            SizedBox(
              height: 100,
            ),
                  Text(
                      "COVID ASSISTANT",
                      style: TextStyle(
                        color: Color.fromRGBO(23, 194, 236, 1),
                        fontSize: 20,
                        fontWeight: FontWeight.w900,
                        fontFamily: "Poppins",
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Informasi Kesehatan",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
            SizedBox(
              height: 10,
            ),
            Container(
              margin: const EdgeInsets.only(left: 20, right: 20),
              child: Row(              
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
              ],
              ),
              // padding: const EdgeInsets.only(bottom: 12),
            ),
            Column(
              children: [
                SizedBox(
                  height: 400,
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: 5,
                    itemBuilder: (BuildContext context, int index) {
                      return InkWell(
                        // onTap: () {
                        //   Navigator.of(context)
                        //       .pushNamed(kesehatan[index].route);
                        // },
                        child: Stack(
                          children: <Widget>[
                            Container(
                                margin: const EdgeInsets.only(
                                    left: 15, right: 15, bottom: 15, top: 15),
                                width: 200,
                                decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.circular(20),
                                  color: Colors.white,
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.grey
                                            .withOpacity(0.3),
                                        spreadRadius: 0.3,
                                        blurRadius: 5.0)
                                  ],
                                ),
                                child: Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding:
                                          const EdgeInsets.all(4.0),
                                      child: Hero(
                                          tag: kesehatan[index].info,
                                          child: Container(
                                            margin: const EdgeInsets.only(
                                    left: 30, right: 30, bottom: 20, top: 20),
                                            width: 150,
                                            height: 150,
                                            decoration: BoxDecoration(
                                                image: DecorationImage(
                                                    image: AssetImage(
                                                        kesehatan[index]
                                                            .imageUrl),
                                                    fit: BoxFit
                                                        .contain)),
                                          )),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 50, top: 4),
                                      child: Text(
                                        kesehatan[index].info,
                                        style: TextStyle(
                                          color:
                                              const Color(0xff47455f),
                                          fontSize: 16,
                                          fontWeight: FontWeight.w800,
                                          fontFamily: "Poppins",
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 12, top: 16),
                                      child: Row(
                                        children: [
                                          Text(
                                            "Rendah: " + kesehatan[index].rendah,
                                            style: TextStyle(
                                              fontFamily: 'Poppins',
                                              fontSize: 16,
                                              color: AppColors
                                                  .mainTextColor,
                                              fontWeight:
                                                  FontWeight.w500,
                                            ),
                                            textAlign: TextAlign.left,
                                          ),
                                        ],
                                      ),
                                    ),
                                                                        Padding(
                                      padding: const EdgeInsets.only(
                                          left: 12, top: 16),
                                      child: Row(
                                        children: [
                                          Text(
                                            "Normal: " + kesehatan[index].normal,
                                            style: TextStyle(
                                              fontFamily: 'Poppins',
                                              fontSize: 16,
                                              color: AppColors
                                                  .mainTextColor,
                                              fontWeight:
                                                  FontWeight.w500,
                                            ),
                                            textAlign: TextAlign.left,
                                          ),
                                        ],
                                      ),
                                    ),
                                                                        Padding(
                                      padding: const EdgeInsets.only(
                                          left: 12, top: 16),
                                      child: Row(
                                        children: [
                                          Text(
                                            "Tinggi: " + kesehatan[index].tinggi,
                                            style: TextStyle(
                                              fontFamily: 'Poppins',
                                              fontSize: 16,
                                              color: AppColors
                                                  .mainTextColor,
                                              fontWeight:
                                                  FontWeight.w500,
                                            ),
                                            textAlign: TextAlign.left,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                )),
                          ],
                        ),
                      );
                    },
                  ),
                ),
                SizedBox(
                  height: 100,
                ),
              Container(
                decoration: BoxDecoration(
                  color: Color.fromRGBO(232, 247, 252, 1),
                ),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 70,
                    ),
                  Text(
                      "COVID ASSISTANT",
                      style: TextStyle(
                        color: Color.fromRGBO(23, 194, 236, 1),
                        fontSize: 20,
                        fontWeight: FontWeight.w900,
                        fontFamily: "Poppins",
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Summary Health Records",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 20.0, horizontal: 40.0),
                      child: HealthRecordsCard(
                        hari: '1',
                        tanggal: '29 Desember 2021',
                        suhu: '35' + '°C',
                        saturasi: '80' + 'mmHg',
                        detak: '50' + 'mg/dl',
                        sistolik: '89' + 'mg/dl',
                        diastolik: '60' + 'mg/dl',
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 4.0, horizontal: 40.0),
                      child: HealthRecordsCard(
                        hari: '2',
                        tanggal: '30 Desember 2021',
                        suhu: '39' + '°C',
                        saturasi: '90' + 'mmHg',
                        detak: '60' + 'mg/dl',
                        sistolik: '79' + 'mg/dl',
                        diastolik: '80' + 'mg/dl',
                      ),
                    ),
                    SizedBox(height: 100),
              ],
            ),
              ),
            ],
            ),
            ],
        ),
      ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).pop();
        },
        backgroundColor: Colors.cyan,
        child: Icon(Icons.keyboard_arrow_left),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }
}
