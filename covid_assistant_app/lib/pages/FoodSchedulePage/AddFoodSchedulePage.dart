import 'dart:convert';
import 'package:covid_assistant_app/Screen/drawer/side_bar_drawer.dart';
import 'package:covid_assistant_app/pages/landingPage/LandingPageAddFoodSchedule.dart';
import 'package:covid_assistant_app/widgets/formFoodSchedule/formFoodSchedule.dart';
import 'package:covid_assistant_app/constants/colors.dart';
import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;

class AddFoodSchedulePage extends StatelessWidget {
  static const routeName = '/tambah-foodschedule';
  static const url = "http://10.0.2.2:8000/json_function";
  const AddFoodSchedulePage({Key? key}) : super(key: key);
  Future<List<dynamic>> _fecthDataUsers() async {
    var result = await http.get(Uri.parse(url));
    print(result);
    print("ini body");
    print(result.body);
    print(jsonDecode(result.body)["data"]);
    return json.decode(result.body)['data'];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(6, 12, 35, 1),
        centerTitle: true,
        title: Text("Tambahkan Catatan Makanan dan Minum"),
      ),
      extendBody: true,
      drawer: DrawerScreen(),
      body: Container(
        decoration: BoxDecoration(
          color: Color.fromRGBO(6, 12, 35, 1),
        ),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              // Navbar(),
              Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 20.0, horizontal: 20),
                child: LandingPageFoodSchedule(),
              ),
              Container(
                // width = MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  color: Color.fromRGBO(232, 247, 252, 1),
                ),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 50.0, horizontal: 40.0),
                      child: FormFoodSchedule(),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              
              ),
            ]
          )
        )
      )
    );
  }
}
