import 'package:covid_assistant_app/pages/landingPage/LandingPageAddFoodSchedule.dart';
import 'package:covid_assistant_app/pages/FoodSchedulePage/AddFoodSchedulePage.dart';
import 'package:covid_assistant_app/widgets/cardFoodSchedule/cardFoodSchedule.dart';

import 'package:covid_assistant_app/Screen/drawer/side_bar_drawer.dart';
import 'package:covid_assistant_app/constants/colors.dart';
import 'package:flutter/material.dart';

class FoodSchedulePage extends StatelessWidget {
  static const routeName = "/food-schedule";
  const FoodSchedulePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(6, 12, 35, 1),
        centerTitle: true,
        title: Text("Food Schedule"),
      ),
      extendBody: true,
      drawer: DrawerScreen(),
      body: Container(
        decoration: BoxDecoration(
          color: Color.fromRGBO(6, 12, 35, 1),
        ),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              // Navbar(),
              Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 20.0, horizontal: 20),
                child: LandingPageFoodSchedule(),
              ),
              MaterialButton(
              color: Color.fromRGBO(6, 12, 35, 1),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(40.0)),
                  side: BorderSide(color: Color.fromRGBO(23, 194, 236, 1))),
              onPressed: () {
                 Navigator.of(context).pushNamed(AddFoodSchedulePage.routeName);
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 12.0, horizontal: 16.0),
                child: Text(
                  "Tambahkan",
                  style: TextStyle(
                      color: Color.fromRGBO(23, 194, 236, 1), fontSize: 14.0),
                ),
              ),
            ),
            SizedBox(
                  height: 100,
                ),
              Container(
                decoration: BoxDecoration(
                  color: Color.fromRGBO(232, 247, 252, 1),
                ),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 70,
                    ),
                  Text(
                      "COVID ASSISTANT",
                      style: TextStyle(
                        color: Color.fromRGBO(23, 194, 236, 1),
                        fontSize: 20,
                        fontWeight: FontWeight.w900,
                        fontFamily: "Poppins",
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Food Schedule",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 20.0, horizontal: 40.0),
                      child: FoodScheduleCard(
                        tanggal: '25 Desember 2021',
                        waktu: '12:49 a.m.',
                        makan: 'ayam',
                        minum: 'air',
                      ),
                    ),
                    
                    SizedBox(height: 100),
              ],
            ),
              ),
            ]
          )
      )
      )
      );
  }
}