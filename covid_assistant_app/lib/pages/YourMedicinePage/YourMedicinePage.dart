import 'package:covid_assistant_app/constants/colors.dart';
import 'package:flutter/material.dart';

class YourMedicinePage extends StatelessWidget {
  static const routeName = "/your-medicine";
  const YourMedicinePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundColor,
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(6, 12, 35, 1),
        centerTitle: true,
        title: Text("Your Medicine"),
      ),
      body: Center(
        child: Text("Hello World!!"),
      ),
    );
  }
}
