import 'package:covid_assistant_app/pages/SignUpPage/SignUpPage.dart';
import 'package:covid_assistant_app/widgets/formLogin/formLogin.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  static const routeName = '/login';

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(6, 12, 35, 1),
        centerTitle: true,
        automaticallyImplyLeading: false,
        // Untuk menghilangkan baris dibawah appbar
        elevation: 0,
        title: Image.asset(
          "assets/images/logo.png",
          width: 180,
        ),
      ),
      extendBody: true,
      backgroundColor: Color.fromRGBO(6, 12, 35, 1),
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            width: 300,
            height: 500,
            decoration: BoxDecoration(
                color: Color.fromRGBO(232, 247, 252, 1),
                borderRadius: BorderRadius.circular(20)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "LOGIN",
                  style: TextStyle(
                    color: Colors.black87,
                    fontSize: 20,
                    fontWeight: FontWeight.w700,
                    fontFamily: "Poppins",
                  ),
                ),
                FormLogin(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Don't have an account?",
                      style: TextStyle(
                        color: Colors.black87,
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        fontFamily: "Poppins",
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8),
                      child: InkWell(
                        child: Text(
                          "Sign Up",
                          style: TextStyle(
                            color: Color.fromRGBO(0, 123, 255, 1),
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            fontFamily: "Poppins",
                          ),
                        ),
                        onTap: () {
                          Navigator.of(context).pushReplacement(
                              MaterialPageRoute(
                                  builder: (context) => SignUpPage()));
                        },
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
