import 'package:covid_assistant_app/constants/colors.dart';
import 'package:covid_assistant_app/pages/YourRoutinePage/AddYourRoutinePage.dart';
import 'package:flutter/material.dart';

import 'package:covid_assistant_app/widgets/cardYourRoutine/cardYourRoutine.dart';
import 'package:covid_assistant_app/Screen/drawer/side_bar_drawer.dart';
import 'package:covid_assistant_app/pages/landingPage/LandingPageYourRoutine.dart';

class YourRoutinePage extends StatelessWidget {
  static const routeName = "/your-routine";
  const YourRoutinePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundColor,
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(6, 12, 35, 1),
        centerTitle: true,
        title: Text("Your Routine"),
      ),  extendBody: true,
      drawer: DrawerScreen(),
      body: Container(
        decoration: BoxDecoration(
          color: Color.fromRGBO(6, 12, 35, 1),
        ),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              // Navbar(),
              Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 20.0, horizontal: 20),
                child: LandingPageYourRoutine(),
              ),
              MaterialButton(
              color: Color.fromRGBO(6, 12, 35, 1),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(40.0)),
                  side: BorderSide(color: Color.fromRGBO(23, 194, 236, 1))),
              onPressed: () {
                 Navigator.of(context).pushNamed(AddYourRoutinePage.routeName);
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 12.0, horizontal: 16.0),
                child: Text(
                  "Tambah Rutinitas",
                  style: TextStyle(
                      color: Color.fromRGBO(23, 194, 236, 1), fontSize: 14.0),
                ),
              ),
            ),
            SizedBox(
              height: 100,
            ),
              Container(
                decoration: BoxDecoration(
                  color: Color.fromRGBO(232, 247, 252, 1),
                ),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 70,
                    ),
                  Text(
                      "COVID ASSISTANT",
                      style: TextStyle(
                        color: Color.fromRGBO(23, 194, 236, 1),
                        fontSize: 20,
                        fontWeight: FontWeight.w900,
                        fontFamily: "Poppins",
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Semua Rutinitas",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 20.0, horizontal: 40.0),
                      child: YourRoutineCard(
                        date: '31 Desember 2021',
                        aktivitas: '08:00   SDA',
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 4.0, horizontal: 40.0),
                      child: YourRoutineCard(
                        date: '31 Desember 2021',
                        aktivitas: '13:00    Alin',
                      ),
                    ),
                    SizedBox(height: 100),
              ],
            ),
              ),
            ],
            ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).pop();
        },
        backgroundColor: Colors.cyan,
        child: Icon(Icons.keyboard_arrow_left),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }
}
     