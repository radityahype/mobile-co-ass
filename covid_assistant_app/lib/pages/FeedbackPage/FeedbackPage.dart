import 'dart:convert';

import 'package:covid_assistant_app/api/models/FeedbackModel.dart';
import 'package:covid_assistant_app/constants/colors.dart';
import 'package:covid_assistant_app/widgets/card/cardFeedback.dart';
import 'package:flutter/material.dart';
import 'package:covid_assistant_app/Screen/drawer/side_bar_drawer.dart';
import 'package:covid_assistant_app/pages/landingPage/LandingPageFeedback.dart';

import 'package:http/http.dart' as http;

class FeedbackPage extends StatelessWidget {
  static const routeName = '/feedback';
  static const url = "https://co-ass.herokuapp.com/json_function";

  // Url jika run menggunakan web lokal
  // static const urlWebLokal = "http://127.0.0.1:8000/json_function";

  //URl jika run menggunakan emulator
  // static const urlMobile = 'http://10.0.2.2:8000/json_function';

  List<FeedbackMessage> users = [];
  Future getUserData() async {
    users = [];
    var response = await http.get(Uri.parse(url));
    var jsonData = jsonDecode(response.body);

    for (var i in jsonData) {
      Fields fields = Fields(
        name: i["fields"]["name"],
        message: i["fields"]["message"],
      );
      FeedbackMessage feedback =
          FeedbackMessage(model: i["model"], pk: i["pk"], fields: fields);
      users.add(feedback);
    }
    return users;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(6, 12, 35, 1),
        centerTitle: true,
        title: Text("Feedback"),
      ),
      extendBody: true,
      drawer: DrawerScreen(),
      body: Container(
        decoration: BoxDecoration(
          color: AppColors.backgroundColor,
        ),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              // Navbar(),
              Container(
                decoration: BoxDecoration(color: Color.fromRGBO(6, 12, 35, 1)),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 20.0, horizontal: 40.0),
                      child: LandingPageFeedback(),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 100,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 40.0),
                child: Text(
                  "FEEDBACK",
                  style: TextStyle(
                    color: Color.fromRGBO(23, 194, 236, 1),
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SizedBox(height: 10),
              Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 20.0, horizontal: 40.0),
                child: SingleChildScrollView(
                  child: FutureBuilder(
                      future: getUserData(),
                      builder: (context, AsyncSnapshot snapshot) {
                        if (snapshot.data == null) {
                          return Container(
                            child: Center(
                                child: Text(
                              "Loading...",
                            )),
                          );
                        } else {
                          return Column(
                              children: users.map((data) {
                            return Column(
                              children: [
                                BuildCard(
                                  from: data.fields.name,
                                  message: data.fields.message,
                                ),
                              ],
                            );
                          }).toList());
                        }
                      }),
                ),
              ),
              SizedBox(height: 120),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).pop();
        },
        backgroundColor: Colors.cyan,
        child: Icon(Icons.keyboard_arrow_left),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }
}
