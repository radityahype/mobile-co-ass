import 'package:covid_assistant_app/pages/DailyJournalPage/AddDailyJournalPage.dart';
import 'package:covid_assistant_app/pages/DailyJournalPage/DailyJournalPage.dart';
import 'package:covid_assistant_app/pages/FeedbackPage/FeedbackPage.dart';
import 'package:covid_assistant_app/pages/FoodSchedulePage/AddFoodSchedulePage.dart';
import 'package:covid_assistant_app/pages/FoodSchedulePage/FoodSchedulePage.dart';
import 'package:covid_assistant_app/pages/HealthRecordsPage/HealthRecordsPage.dart';
import 'package:covid_assistant_app/pages/HealthRecordsPage/AddHealthRecordsPage.dart';
import 'package:covid_assistant_app/pages/LoginPage/LoginPage.dart';
import 'package:covid_assistant_app/pages/SignUpPage/SignUpPage.dart';
import 'package:covid_assistant_app/pages/SwabRecordsPage/AddSwabRecordsPage.dart';
import 'package:covid_assistant_app/pages/SwabRecordsPage/SwabRecordsPage.dart';
import 'package:covid_assistant_app/pages/YourMedicinePage/YourMedicinePage.dart';
import 'package:covid_assistant_app/pages/YourRoutinePage/AddYourRoutinePage.dart';
import 'package:covid_assistant_app/pages/YourRoutinePage/YourRoutinepage.dart';
import 'package:covid_assistant_app/pages/homePage/homePage.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'cookies.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provider(
        create: (_) {
          CookieRequest request = CookieRequest();

          return request;
        },
        child: MaterialApp(
          title: 'Covid Assistant',
          theme: ThemeData(
            primarySwatch: Colors.blue,
            fontFamily: "Poppins",
          ),
          // home: MyHomePage(),
          // InitialRoute digunakan untuk menampilkan page pertama kali ketika dirun
          initialRoute: LoginPage.routeName,
          routes: {
            MyHomePage.routeName: (context) => MyHomePage(),
            FeedbackPage.routeName: (context) => FeedbackPage(),
            LoginPage.routeName: (context) => LoginPage(),
            SignUpPage.routeName: (context) => SignUpPage(),
            DailyJournalPage.routeName: (context) => const DailyJournalPage(),
            AddDailyJournalPage.routeName: (context) =>
                const AddDailyJournalPage(),
            HealthRecordsPage.routeName: (context) => const HealthRecordsPage(),
            AddHealthRecordsPage.routeName: (context) =>
                const AddHealthRecordsPage(),
            FoodSchedulePage.routeName: (context) => const FoodSchedulePage(),
            AddFoodSchedulePage.routeName: (context) => 
                const AddFoodSchedulePage(),
            SwabRecordsPage.routeName: (context) => const SwabRecordsPage(),
            AddSwabRecordsPage.routeName: (context) =>
                const AddSwabRecordsPage(),
            YourMedicinePage.routeName: (context) => const YourMedicinePage(),
            YourRoutinePage.routeName: (context) => const YourRoutinePage(),
          },
        ));
  }
}
