import 'package:covid_assistant_app/pages/LoginPage/LoginPage.dart';
import 'package:flutter/material.dart';
import 'package:covid_assistant_app/Screen/drawer/navigation_drawer_header.dart';
import 'package:covid_assistant_app/pages/FeedbackPage/FeedbackPage.dart';
import 'package:covid_assistant_app/pages/homePage/homePage.dart';
import 'package:provider/src/provider.dart';

import '../../cookies.dart';

class DrawerScreen extends StatefulWidget {
  const DrawerScreen({Key? key}) : super(key: key);

  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    final request = context.watch<CookieRequest>();
    return Drawer(
      child: ListView(
        children: <Widget>[
          NavigationHeader(),
          // UserAccountsDrawerHeader(
          //   accountName: Text("Radit"),
          //   accountEmail: Text("radityahype@compfest.id"),

          // ),
          SizedBox(
            height: 20,
          ),
          DrawerListTile(
            iconData: Icons.home,
            title: "Home",
            onClicked: () => selectedItem(context, 0),
          ),
          SizedBox(
            height: 20,
          ),
          DrawerListTile(
            iconData: Icons.library_books_sharp,
            title: "Feedback",
            onClicked: () => selectedItem(context, 1),
          ),
          SizedBox(
            height: 20,
          ),
          request.username!= ''?
          DrawerListTile(
            iconData: Icons.logout,
            title: "Logout",
             onClicked: () async {
            final response = await request.logoutAccount("https://co-ass.herokuapp.com/authentication/logout-flutter/");
            if (response['status']) {
              ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                content: Text("Successfully logged out!"),
              ));
              Navigator.pushReplacementNamed(
                  context, LoginPage.routeName);
            } else {
              ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                content: Text("An error occured, please try again."),
              ));
            }
             })
          : DrawerListTile(
            iconData: Icons.login,
            title: "Login",
            onClicked: () => selectedItem(context, 2),
          ),
        ],
      ),
    );
  }
}

void selectedItem(BuildContext context, int index) {
  Navigator.of(context).pop();

  switch (index) {
    case 0:
      Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => MyHomePage(),
      ));
      break;
    case 1:
      Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => FeedbackPage(),
      ));
      break;

    case 2:
      Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => LoginPage(),
      ));
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData iconData;
  final String title;
  final VoidCallback onClicked;
  const DrawerListTile(
      {Key? key,
      required this.iconData,
      required this.title,
      required this.onClicked})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onClicked,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title,
        style: TextStyle(fontSize: 16),
      ),
    );
  }
}
