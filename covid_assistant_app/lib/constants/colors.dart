import 'package:flutter/material.dart';

class AppColors {
  // static final Color textColor2 = Color(0xFF989acd);
  static final Color textColor1 = Color(0xFF878593);
  static final Color mainTextColor = Color(0xFFababad);
  static final Color titleTextColor = Colors.black;
  static final Color secondaryTitleTextColor = Color.fromRGBO(23, 194, 236, 1);
  static final Color backgroundColor = Color.fromRGBO(232, 247, 252, 1);
  static final Color buttonBackground = Color(0xFFf1f1f9);
  static final Color mainColor = Color.fromRGBO(6, 12, 35, 1);
}
