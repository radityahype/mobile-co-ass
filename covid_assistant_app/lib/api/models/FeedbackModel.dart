// To parse this JSON data, do
//
//     final feedbackMessage = feedbackMessageFromJson(jsonString);

import 'dart:convert';

List<FeedbackMessage> feedbackMessageFromJson(String str) => List<FeedbackMessage>.from(json.decode(str).map((x) => FeedbackMessage.fromJson(x)));

String feedbackMessageToJson(List<FeedbackMessage> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class FeedbackMessage {
    FeedbackMessage({
        required this.model,
        required this.pk,
        required this.fields,
    });

    String model;
    int pk;
    Fields fields;

    factory FeedbackMessage.fromJson(Map<String, dynamic> json) => FeedbackMessage(
        model: json["model"],
        pk: json["pk"],
        fields: Fields.fromJson(json["fields"]),
    );

    Map<String, dynamic> toJson() => {
        "model": model,
        "pk": pk,
        "fields": fields.toJson(),
    };
}

class Fields {
    Fields({
        required this.name,
        required this.message,
    });

    String name;
    String message;

    factory Fields.fromJson(Map<String, dynamic> json) => Fields(
        name: json["name"],
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "message": message,
    };
}
