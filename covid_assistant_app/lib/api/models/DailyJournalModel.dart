// To parse this JSON data, do
//
//     final dailyJournalMessage = dailyJournalMessageFromJson(jsonString);

import 'dart:convert';

List<DailyJournalMessage> dailyJournalMessageFromJson(String str) => List<DailyJournalMessage>.from(json.decode(str).map((x) => DailyJournalMessage.fromJson(x)));

String dailyJournalMessageToJson(List<DailyJournalMessage> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class DailyJournalMessage {
    DailyJournalMessage({
        required this.model,
        required this.pk,
        required this.fields,
    });

    String model;
    int pk;
    Fields fields;

    factory DailyJournalMessage.fromJson(Map<String, dynamic> json) => DailyJournalMessage(
        model: json["model"],
        pk: json["pk"],
        fields: Fields.fromJson(json["fields"]),
    );

    Map<String, dynamic> toJson() => {
        "model": model,
        "pk": pk,
        "fields": fields.toJson(),
    };
}

class Fields {
    Fields({
        required this.hari,
        required this.tanggal,
        required this.isDemam,
        required this.isBatuk,
        required this.isKelelahan,
        required this.isPenciuman,
        required this.curhat,
        required this.user,
    });

    String hari;
    String tanggal;
    bool isDemam;
    bool isBatuk;
    bool isKelelahan;
    bool isPenciuman;
    String curhat;
    int user;

    factory Fields.fromJson(Map<String, dynamic> json) => Fields(
        hari: json["hari"],
        tanggal: json["tanggal"],
        isDemam: json["is_demam"],
        isBatuk: json["is_batuk"],
        isKelelahan: json["is_kelelahan"],
        isPenciuman: json["is_penciuman"],
        curhat: json["curhat"],
        user: json["user"],
    );

    Map<String, dynamic> toJson() => {
        "hari": hari,
        "tanggal": tanggal,
        "is_demam": isDemam,
        "is_batuk": isBatuk,
        "is_kelelahan": isKelelahan,
        "is_penciuman": isPenciuman,
        "curhat": curhat,
        "user": user,
    };
}
