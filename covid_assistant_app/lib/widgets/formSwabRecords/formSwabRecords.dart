import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:covid_assistant_app/pages/SwabRecordsPage/SwabRecordsPage.dart';

import 'package:http/http.dart' as http;

class FormSwabRecords extends StatefulWidget {
  State<StatefulWidget> createState() {
    return FormSwabRecordsState();
  }
}

class FormSwabRecordsState extends State<FormSwabRecords> {
  String? tanggal;
  String? hasil;
  String? ctValue;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Widget _buildTanggal() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Tanggal', hintText: "Masukkan tanggal swab"),
      maxLength: 20,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Tanggal is Required';
        }

        return null;
      },
      onSaved: (String? value) {
        tanggal = value!;
      },
    );
  }

  Widget _buildHasil() {
    return TextFormField(
      decoration:
          InputDecoration(labelText: 'Hasil', hintText: "Masukkan hasil swab"),
      maxLength: 20,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Hasil is Required';
        }

        return null;
      },
      onSaved: (String? value) {
        hasil = value!;
      },
    );
  }

  Widget _buildCTValue() {
    return TextFormField(
      decoration:
          InputDecoration(labelText: 'CT Value', hintText: "Masukkan CT Value"),
      maxLength: 20,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'CT value is Required';
        }

        return null;
      },
      onSaved: (String? value) {
        ctValue = value!;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(24),
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            _buildTanggal(),
            _buildHasil(),
            _buildCTValue(),
            SizedBox(height: 60),
            MaterialButton(
              color: Color.fromRGBO(6, 12, 35, 1),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(14.0)),
                  side: BorderSide(color: Color.fromRGBO(23, 194, 236, 1))),
              onPressed: () async {
                if (!_formKey.currentState!.validate()) {
                  return;
                }

                _formKey.currentState!.save();
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('Berhasil dikirim!')),
                );
                print("Data");
                print("Tanggal: " + tanggal!);
                print("Hasil: " + hasil!);
                print("CT Value: " + ctValue!);
                final response = await http.post(Uri.parse("http://127.0.0.1:8000/fetch_feedback"),
                headers: <String, String>{
                  'Content-Type': 'application/json;charset=UTF-8'},
                body: jsonEncode(<String, String>{
                  'tanggal': tanggal!,
                  'hasil': hasil!,
                  'ct value': ctValue!,
                })
                );
                print(response);
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 14.0),
                child: Text(
                  "Tambahkan",
                  style: TextStyle(color: Colors.white, fontSize: 14.0),
                ),
              ),
            ),
            SizedBox(height: 20),
            MaterialButton(
              color: Color.fromRGBO(6, 12, 35, 1),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(14.0)),
                  side: BorderSide(color: Color.fromRGBO(23, 194, 236, 1))),
              onPressed: () {
                Navigator.of(context).pushNamed(SwabRecordsPage.routeName);
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 14.0),
                child: Text(
                  "Lihat riwayat swab",
                  style: TextStyle(color: Colors.white, fontSize: 14.0),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            )
            // onPressed: () {
            //   if (!_formKey.currentState!.validate()) {
            //     return;
            //   }

            //   _formKey.currentState!.save();

            //   print(_name);
            //   print(_message);

            //   //Send to API
            // },
          ],
        ),
      ),
    );
  }
}
