import 'package:covid_assistant_app/pages/homePage/homePage.dart';
import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';

import '../../cookies.dart';

class FormLogin extends StatefulWidget {
  State<StatefulWidget> createState() {
    return _FormLoginState();
  }
}

class _FormLoginState extends State<FormLogin> {
  String? _username;
  String? _password;
  bool isHiddenPassword = true;
  Icon iconPassword = Icon(Icons.visibility_off);
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Widget _buildName() {
    return TextFormField(
      decoration:
          InputDecoration(labelText: 'Username', hintText: "Masukkan username"),
      maxLength: 20,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Username is Required';
        }

        return null;
      },
      onChanged: (String? value) {
        _username = value!;
      },
    );
  }

  Widget _buildPassword() {
    return TextFormField(
      obscureText: isHiddenPassword,
      decoration: InputDecoration(
          labelText: 'Password',
          hintText: "Masukkan password",
          suffixIcon: InkWell(onTap: _togglePasswordView, child: iconPassword)),
      maxLength: 200,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Password is Required';
        }

        return null;
      },
      onChanged: (String? value) {
        _password = value!;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final request = context.watch<CookieRequest>();
    return Container(
      margin: EdgeInsets.all(24),
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            _buildName(),
            _buildPassword(),
            SizedBox(height: 60),
            MaterialButton(
              color: Color.fromRGBO(6, 12, 35, 1),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(14.0)),
                  side: BorderSide(color: Color.fromRGBO(23, 194, 236, 1))),
              onPressed: () async {
                try {
                  final response = await request.login(
                      "https://co-ass.herokuapp.com/authentication/login-flutter/",
                      {
                        'username': _username,
                        'password': _password,
                      });
                  print(response);
                  if (request.loggedIn) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Text(response["message"]),
                      ),
                    );
                    Navigator.pushNamed(context, MyHomePage.routeName);
                  } else {
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Text(response["message"]),
                      ),
                    );
                  }
                } catch (e) {
                  print(e);
                }
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 14.0),
                child: Text(
                  "Login",
                  style: TextStyle(color: Colors.white, fontSize: 14.0),
                ),
              ),
            ),
            SizedBox(height: 20),
          ],
        ),
      ),
    );
  }

  void _togglePasswordView() {
    if (isHiddenPassword == true) {
      isHiddenPassword = false;
      iconPassword = Icon(Icons.visibility);
    } else {
      isHiddenPassword = true;
      iconPassword = Icon(Icons.visibility_off);
    }
    setState(() {});
  }
}
