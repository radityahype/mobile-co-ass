import 'package:covid_assistant_app/pages/DailyJournalPage/DailyJournalPage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/src/provider.dart';

import '../../cookies.dart';


class FormDailyJournal extends StatefulWidget {
  State<StatefulWidget> createState() {
    return FormDailyJournalState();
  }
}


class FormDailyJournalState extends State<FormDailyJournal> {
  String? hari;
  String? tanggal;
  String? demam;
  String? batuk;
  String? kelelahan;
  String? kehilanganPenciuman;
  String? keluhan;

  TextEditingController _hari = TextEditingController();
  TextEditingController _tanggal = TextEditingController();
  TextEditingController _demam = TextEditingController();
  TextEditingController _batuk = TextEditingController();
  TextEditingController _kelelahan = TextEditingController();
  TextEditingController _kehilanganPenciuman = TextEditingController();
  TextEditingController _keluhan = TextEditingController();


  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
   final request = context.watch<CookieRequest>();
    return Container(
      margin: EdgeInsets.all(24),
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            TextFormField(
            controller: _hari,
            decoration: InputDecoration(labelText: 'Hari ke-', hintText: "Isi dengan angka, misal 1, 2, 3, dst."),
            maxLength: 2,
            validator: (String? value) {
                if (value!.isEmpty) {
                return 'Hari harus diisi!';
            }

        return null;
      },
      onSaved: (String? value) {
        hari = value!;
      },
    ),
            TextFormField(
              controller: _tanggal,
      decoration:
          InputDecoration(labelText: 'Tanggal', hintText: "YYYY-MM-DD"),
      maxLength: 10,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Tanggal harus diisi!';
        }

        return null;
      },
      onSaved: (String? value) {
        tanggal = value!;
      },
    ),
            TextFormField(
              controller: _demam,
      decoration:
          InputDecoration(labelText: 'Apakah anda mengalami demam hari ini?', hintText: "Ya/Tidak"),
      maxLength: 5,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Textfield harus diisi!';
        }

        return null;
      },
      onSaved: (String? value) {
        demam = value!;
      },
    ),
            TextFormField(
              controller: _batuk ,
      decoration:
          InputDecoration(labelText: 'Apakah anda mengalami batuk hari ini?', hintText: "Ya/Tidak"),
      maxLength: 5,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Textdield harus diisi';
        }

        return null;
      },
      onSaved: (String? value) {
        batuk = value!;
      },
    ),
            TextFormField(
              controller: _kelelahan,
      decoration:
          InputDecoration(labelText: 'Apakah anda mengalami kelelahan hari ini?', hintText: "Ya/Tidak"),
      maxLength: 5,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Textdield harus diisi';
        }

        return null;
      },
      onSaved: (String? value) {
        kelelahan = value!;
      },
    ),
TextFormField(
      controller: _kehilanganPenciuman,
      decoration:
          InputDecoration(labelText: 'Apakah anda mengalami kehilangan penciuman hari ini?', hintText: "Ya/Tidak"),
      maxLength: 5,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Textdield harus diisi';
        }

        return null;
      },
      onSaved: (String? value) {
        kehilanganPenciuman = value!;
      },
    ),
            TextFormField(
              controller: _keluhan,
      decoration:
          InputDecoration(labelText: 'Keluhan lainnya', hintText: "Ceritakan apa yang anda rasakan hari ini!"),
      maxLength: 500,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Textdield harus diisi';
        }

        return null;
      },
      onSaved: (String? value) {
        keluhan = value!;
      },
    ),
            SizedBox(height: 60),
            MaterialButton(
              color: Color.fromRGBO(6, 12, 35, 1),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(14.0)),
                  side: BorderSide(color: Color.fromRGBO(23, 194, 236, 1))),
              onPressed: () async {
                if (!_formKey.currentState!.validate()) {
                  return;
                }
                String isDemam = _demam.text == "Ya" ? "True": "False";
                String isBatuk = _batuk.text == "Ya" ? "True": "False";
                String isKelelahan = _kelelahan.text == "Ya" ? "True": "False";
                String isKehilanganPenciuman = _kehilanganPenciuman.text == "Ya" ? "True": "False";
                final http.Response response = await http.post(
                          Uri.parse("https://co-ass.herokuapp.com/daily-journal/post-flutter"),
                          body: {
                            "hari": _hari.text,
                            "tanggal": _tanggal.text,
                            "is_demam": isDemam,
                            "is_batuk": isBatuk,
                            "is_kelelahan": isKelelahan,
                            "is_penciuman": isKehilanganPenciuman,
                            "curhat": _keluhan.text,
                            "user": request.username,

              });
                            print(response.body);
                _formKey.currentState!.save();
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('Feedback Berhasil dikirim!')),
                );
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 14.0),
                child: Text(
                  "Kirim",
                  style: TextStyle(color: Colors.white, fontSize: 14.0),
                ),
              ),
            ),
            SizedBox(height: 20),
            SizedBox(
              height: 20,
            )
            // onPressed: () {
            //   if (!_formKey.currentState!.validate()) {
            //     return;
            //   }

            //   _formKey.currentState!.save();

            //   print(_name);
            //   print(_message);

            //   //Send to API
            // },
          ],
        ),
      ),
    );
  }
}
