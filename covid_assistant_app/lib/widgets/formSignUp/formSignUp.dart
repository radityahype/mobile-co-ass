import 'package:covid_assistant_app/pages/homePage/homePage.dart';
import 'package:flutter/material.dart';

class FormSignUp extends StatefulWidget {
  State<StatefulWidget> createState() {
    return _FormSignUpState();
  }
}

class _FormSignUpState extends State<FormSignUp> {
  String? _username;
  String? _email;
  String? _password;
  // TextEditingController _confirmpassword = TextEditingController();
  String? _confirmPassword;

  bool isHiddenPassword = true;
  Icon iconPassword = Icon(Icons.visibility_off);
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Widget _buildName() {
    return TextFormField(
      decoration:
          InputDecoration(labelText: 'Username', hintText: "Masukkan username"),
      maxLength: 20,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Username is Required';
        }

        return null;
      },
      onSaved: (String? value) {
        _username = value!;
      },
    );
  }

  Widget _buildEmail() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Email'),
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Email is Required';
        }

        if (!RegExp(
                r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
            .hasMatch(value)) {
          return 'Please enter a valid email Address';
        }

        return null;
      },
      onSaved: (String? value) {
        _email = value!;
      },
    );
  }
  // Widget _buildEmail() {
  //   return TextFormField(
  //     decoration:
  //         InputDecoration(labelText: 'Email', hintText: "Masukkan email"),
  //     // maxLength: 20,
  //     validator: (String? value) {
  //       if (value!.isEmpty) {
  //         return 'Email is Required';
  //       }

  //       return null;
  //     },
  //     onSaved: (String? value) {
  //       _username = value!;
  //     },
  //   );
  // }

  Widget _buildPassword() {
    return TextFormField(
      obscureText: isHiddenPassword,
      decoration: InputDecoration(
          labelText: 'Password',
          hintText: "Masukkan password",
          suffixIcon: InkWell(onTap: _togglePasswordView, child: iconPassword)),
      maxLength: 200,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Password is Required';
        }

        return null;
      },
      onSaved: (String? value) {
        _password = value!;
      },
    );
  }

  Widget _buildConfirmPassword() {
    return TextFormField(
      // controller: _confirmpassword,
      obscureText: isHiddenPassword,
      decoration: InputDecoration(
          labelText: 'Confirm Password',
          hintText: "Re-enter Password",
          suffixIcon: InkWell(onTap: _togglePasswordView, child: iconPassword)),
      maxLength: 200,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Password is Required';
        }

        if (_password != _confirmPassword) {
          return "Password does not match";
        }

        return null;
      },

      onSaved: (String? value) {
        _confirmPassword = value!;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(24),
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            _buildName(),
            _buildEmail(),
            _buildPassword(),
            _buildConfirmPassword(),
            SizedBox(height: 60),
            MaterialButton(
              color: Color.fromRGBO(6, 12, 35, 1),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(14.0)),
                  side: BorderSide(color: Color.fromRGBO(23, 194, 236, 1))),
              onPressed: () {
                if (!_formKey.currentState!.validate()) {
                  return;
                }
                _formKey.currentState!.save();
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('Login berhasil')),
                );
                print("Data");
                print("Username: " + _username!);
                print("Email: " + _email!);
                print("password: " + _password!);
                print("Confirm Password: " + _confirmPassword!);
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 14.0),
                child: Text(
                  "Sign Up",
                  style: TextStyle(color: Colors.white, fontSize: 14.0),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _togglePasswordView() {
    if (isHiddenPassword == true) {
      isHiddenPassword = false;
      iconPassword = Icon(Icons.visibility);
    } else {
      isHiddenPassword = true;
      iconPassword = Icon(Icons.visibility_off);
    }
    setState(() {});
  }
}
