import 'package:flutter/material.dart';

class SwabRecordsCard extends StatefulWidget {
  final String tanggal;
  final String hasil;
  final String ctValue;

  const SwabRecordsCard({Key? key, required this.tanggal, required this.hasil, required this.ctValue})
      : super(key: key);

  @override
  SwabRecordsCardState createState() => SwabRecordsCardState();
}

class SwabRecordsCardState extends State<SwabRecordsCard> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.only(top: 12.0),
        child: Card(
          color: Color.fromRGBO(6, 12, 35, 1),
          elevation: 30,
          shadowColor: Color.fromRGBO(23, 194, 236, 1),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(28.0)),
          child: InkWell(
            splashColor: Colors.blue.withAlpha(50),
            borderRadius: BorderRadius.circular(28.0),
            onTap: () {
              print('Card tapped.');
            },
            child: SizedBox(
              child: Column(
                // Untuk menyesuaikan card dengan ukuran mobile
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Padding(
                    padding: EdgeInsets.all(20.0),
                    child: Column(children: [
                      Text(
                        'Tanggal: ',
                        style: TextStyle(
                            color: Color.fromRGBO(187, 187, 187, 1),
                            fontSize: 16,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w500),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: 2,
                      ),
                      Text(
                        widget.tanggal,
                        style: TextStyle(
                            fontSize: 16,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w500,
                            color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        'Hasil: ',
                        style: TextStyle(
                            color: Color.fromRGBO(187, 187, 187, 1),
                            fontSize: 16,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w500),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: 2,
                      ),
                      Text(
                        widget.hasil,
                        style: TextStyle(
                            fontSize: 16,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w500,
                            color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: 4,
                      ),
                      Text(
                        'CT Value: ',
                        style: TextStyle(
                            color: Color.fromRGBO(187, 187, 187, 1),
                            fontSize: 16,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w500),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: 2,
                      ),
                      Text(
                        widget.ctValue,
                        style: TextStyle(
                            fontSize: 16,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.w500,
                            color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: 4,
                      ),
                    ]),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
