import 'package:flutter/material.dart';

class DailyJournalCard extends StatefulWidget {
  final String hari;
  final String tanggal;
  final String demam;
  final String batuk;
  final String kelelahan;
  final String kehilanganPenciuman;
  final String keluhan;

  
  const DailyJournalCard({Key? key, required this.hari, required this.tanggal, required this.demam, required this.batuk, required this.kelelahan, required this.kehilanganPenciuman, required this.keluhan})
      : super(key: key);

  @override
  _DailyJournalCardState createState() => _DailyJournalCardState();
}

class _DailyJournalCardState extends State<DailyJournalCard> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        color: Color.fromRGBO(6, 12, 35, 1),
        elevation: 30,
        shadowColor: Color.fromRGBO(23, 194, 236, 1),
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(28.0)),
        child: InkWell(
          splashColor: Colors.blue.withAlpha(50),
          borderRadius: BorderRadius.circular(28.0),
          onTap: () {
            print('Card tapped.');
          },
          child: SizedBox(
            child: Column(
              // Untuk menyesuaikan card dengan ukuran mobile
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: Column(children: [
                    Text(
                      'Hari ke-: ',
                      style: TextStyle(
                          color: Color.fromRGBO(187, 187, 187, 1),
                          fontSize: 20,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Text(
                      widget.hari,
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w500,
                          color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    Text(
                      'Tanggal: ',
                      style: TextStyle(
                          color: Color.fromRGBO(187, 187, 187, 1),
                          fontSize: 16,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Text(
                      widget.tanggal,
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w500,
                          color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                     SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Demam: ',
                      style: TextStyle(
                          color: Color.fromRGBO(187, 187, 187, 1),
                          fontSize: 16,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Text(
                      widget.demam,
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w500,
                          color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Batuk: ',
                      style: TextStyle(
                          color: Color.fromRGBO(187, 187, 187, 1),
                          fontSize: 16,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Text(
                      widget.batuk,
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w500,
                          color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Kelelahan: ',
                      style: TextStyle(
                          color: Color.fromRGBO(187, 187, 187, 1),
                          fontSize: 16,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Text(
                      widget.kelelahan,
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w500,
                          color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Kehilangan Penciuman: ',
                      style: TextStyle(
                          color: Color.fromRGBO(187, 187, 187, 1),
                          fontSize: 16,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Text(
                      widget.kehilanganPenciuman,
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w500,
                          color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Curhat: ',
                      style: TextStyle(
                          color: Color.fromRGBO(187, 187, 187, 1),
                          fontSize: 16,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Text(
                      widget.keluhan,
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w500,
                          color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                  ]),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}