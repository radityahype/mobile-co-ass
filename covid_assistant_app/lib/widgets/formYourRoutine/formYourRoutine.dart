import 'dart:convert';

import 'package:covid_assistant_app/pages/YourRoutinePage/YourRoutinepage.dart';
import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;

class FormYourRoutine extends StatefulWidget {
  State<StatefulWidget> createState() {
    return FormYourRoutineState();
  }
}

class FormYourRoutineState extends State<FormYourRoutine> {
  String? date;
  String? aktivitas;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Widget _buildDate() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Tanggal', hintText: "dd/mm/yy"),
      maxLength: 20,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Tanggal is Required';
        }
        return null;
      },
      onSaved: (String? value) {
        date = value!;
      },
    );
  }

  Widget _buildAktivitas() {
    return TextFormField(
      decoration:
          InputDecoration(labelText: 'Masukkan Rutinitas', hintText: "08:00 Berjemur \n 10:00 Brunch \n 13:00 Meeting \n 15:00 Olahraga \n 19:00 Nonton Film"),
      maxLength: 2000,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Rutinitas is Required';
        }
        return null;
      },
      onSaved: (String? value) {
        aktivitas = value!;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(24),
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            _buildDate(),
            _buildAktivitas(),
            SizedBox(height: 60),
            MaterialButton(
              color: Color.fromRGBO(6, 12, 35, 1),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(14.0)),
                  side: BorderSide(color: Color.fromRGBO(23, 194, 236, 1))),
              onPressed: () async {
                if (!_formKey.currentState!.validate()) {
                  return;
                }

                _formKey.currentState!.save();
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('Berhasil dikirim!')),
                );
                print("Data");
                print("Tanggal: " + date!);
                print("Rutinitas: " + aktivitas!);
                final response = await http.post(Uri.parse("http://127.0.0.1:8000/fetch_feedback"),
                headers: <String, String>{
                  'Content-Type': 'application/json;charset=UTF-8'},
                body: jsonEncode(<String, String>{
                  'tanggal': date!,
                  'rutinitas': aktivitas!,
                })
                );
                print(response);
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 14.0),
                child: Text(
                  "Tambahkan",
                  style: TextStyle(color: Colors.white, fontSize: 14.0),
                ),
              ),
            ),
            SizedBox(height: 20),
            MaterialButton(
              color: Color.fromRGBO(6, 12, 35, 1),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(14.0)),
                  side: BorderSide(color: Color.fromRGBO(23, 194, 236, 1))),
              onPressed: () {
                Navigator.of(context).pushNamed(YourRoutinePage.routeName);
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 14.0),
                child: Text(
                  "Daftar Rutinitas",
                  style: TextStyle(color: Colors.white, fontSize: 14.0),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            )
          ],
        ),
      ),
    );
  }
}
