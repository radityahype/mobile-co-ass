import 'package:covid_assistant_app/pages/DailyJournalPage/DailyJournalPage.dart';
import 'package:flutter/material.dart';

// List<String namafitur = [
//   "Your Medicine",
//   "Your Routine",
//   "Food Schedule",
//   "Health Records",
//   "Swab Records",
//   "Daily Journal",
//
// ];
class InfoFitur {
  final int position;
  final String namaFitur;
  final String imageUrl;
  final String route;

  InfoFitur(this.position,
      {required this.namaFitur, required this.imageUrl, required this.route});
}

List<InfoFitur> fitur = [
  InfoFitur(1,
      namaFitur: "Your Medicine",
      imageUrl: "assets/images/fitur-1.png",
      route: "/your-medicine"),
  InfoFitur(2,
      namaFitur: "Your Routine",
      imageUrl: "assets/images/fitur-2.png",
      route: "/your-routine"),
  InfoFitur(3,
      namaFitur: "Food Schedule",
      imageUrl: "assets/images/fitur-3.png",
      route: "/food-schedule"),
  InfoFitur(4,
      namaFitur: "Health Records",
      imageUrl: "assets/images/fitur-4.png",
      route: "/health-records"),
  InfoFitur(5,
      namaFitur: "Swab Records",
      imageUrl: "assets/images/fitur-5.png",
      route: "/swab-records"),
  InfoFitur(6,
      namaFitur: "Daily Journal",
      imageUrl: "assets/images/fitur-6.png",
      route: "/daily-journal"),
];
