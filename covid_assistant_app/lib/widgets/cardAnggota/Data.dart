import 'package:flutter/material.dart';

// List<String namaAnggota = [
//   "Raditya Hanif Yudha P.",
//   "Karlina Rana Salsabila",
//   "Danela Syafika Desideria",
//   "Muhammad Athif",
//   "Hilmi Al-biruni",
//   "Amanda Putri Khairunnisa",
//   "Zidan Amukti Rajendra",
// ];

class InfoNamaAnggota {
  final int position;
  final String name;
  final String imageUrl;
  final String description;

  InfoNamaAnggota(
    this.position, {
    required this.name,
    required this.imageUrl,
    required this.description,
  });
}

List<InfoNamaAnggota> namaAnggota = [
  InfoNamaAnggota(
    1,
    name: "Raditya Hanif \nYudha P.\n",
    imageUrl: "assets/images/radit.png",
    description: "Jurusan Sistem Informasi Fakultas Ilmu Komputer UI 2020",
  ),
  InfoNamaAnggota(
    2,
    name: "Karlina Rana Salsabila\n",
    imageUrl: "assets/images/Karlina.png",
    description: "Jurusan Sistem Informasi Fakultas Ilmu Komputer UI 2020",
  ),
  InfoNamaAnggota(
    3,
    name: "Danela Syafika Desideria\n",
    imageUrl: "assets/images/danela.png",
    description: "Jurusan Sistem Informasi Fakultas Ilmu Komputer UI 2020",
  ),
  InfoNamaAnggota(
    4,
    name: "Muhammad \nAthif\n",
    imageUrl: "assets/images/athif.png",
    description: "Jurusan Sistem Informasi Fakultas Ilmu Komputer UI 2020",
  ),
  InfoNamaAnggota(
    5,
    name: "Hilmi Al-\nbiruni\n",
    imageUrl: "assets/images/hilmi.png",
    description: "Jurusan Sistem Informasi Fakultas Ilmu Komputer UI 2020",
  ),
  InfoNamaAnggota(
    6,
    name: "Amanda Putri Khairunnisa\n",
    imageUrl: "assets/images/Manda.png",
    description: "Jurusan Sistem Informasi Fakultas Ilmu Komputer UI 2020",
  ),
  InfoNamaAnggota(
    7,
    name: "Zidan Amukti Rajendra\n",
    imageUrl: "assets/images/zidan.png",
    description: "Jurusan Sistem Informasi Fakultas Ilmu Komputer UI 2020",
  ),
];
