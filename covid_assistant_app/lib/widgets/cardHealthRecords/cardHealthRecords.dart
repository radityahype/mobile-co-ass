import 'package:flutter/material.dart';

class HealthRecordsCard extends StatefulWidget {
  final String hari;
  final String tanggal;
  final String suhu;
  final String saturasi;
  final String detak;
  final String sistolik;
  final String diastolik;

  
  const HealthRecordsCard({Key? key, required this.hari, required this.tanggal, required this.suhu, required this.saturasi, required this.detak, required this.sistolik, required this.diastolik})
      : super(key: key);

  @override
  _HealthRecordsCardState createState() => _HealthRecordsCardState();
}

class _HealthRecordsCardState extends State<HealthRecordsCard> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        color: Color.fromRGBO(6, 12, 35, 1),
        elevation: 30,
        shadowColor: Color.fromRGBO(23, 194, 236, 1),
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(28.0)),
        child: InkWell(
          splashColor: Colors.blue.withAlpha(50),
          borderRadius: BorderRadius.circular(28.0),
          onTap: () {
            print('Card tapped.');
          },
          child: SizedBox(
            child: Column(
              // Untuk menyesuaikan card dengan ukuran mobile
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: Column(children: [
                    Text(
                      'Hari ke: ',
                      style: TextStyle(
                          color: Color.fromRGBO(187, 187, 187, 1),
                          fontSize: 20,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Text(
                      widget.hari,
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w500,
                          color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    Text(
                      'Tanggal: ',
                      style: TextStyle(
                          color: Color.fromRGBO(187, 187, 187, 1),
                          fontSize: 16,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Text(
                      widget.tanggal,
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w500,
                          color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                     SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Suhu: ',
                      style: TextStyle(
                          color: Color.fromRGBO(187, 187, 187, 1),
                          fontSize: 16,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Text(
                      widget.suhu,
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w500,
                          color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Saturasi: ',
                      style: TextStyle(
                          color: Color.fromRGBO(187, 187, 187, 1),
                          fontSize: 16,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Text(
                      widget.saturasi,
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w500,
                          color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Detak Jantung: ',
                      style: TextStyle(
                          color: Color.fromRGBO(187, 187, 187, 1),
                          fontSize: 16,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Text(
                      widget.detak,
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w500,
                          color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Sistolik: ',
                      style: TextStyle(
                          color: Color.fromRGBO(187, 187, 187, 1),
                          fontSize: 16,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Text(
                      widget.sistolik,
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w500,
                          color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Diastolik: ',
                      style: TextStyle(
                          color: Color.fromRGBO(187, 187, 187, 1),
                          fontSize: 16,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Text(
                      widget.diastolik,
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: "Poppins",
                          fontWeight: FontWeight.w500,
                          color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                  ]),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}