import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:covid_assistant_app/pages/HealthRecordsPage/HealthRecordsPage.dart';

import 'package:http/http.dart' as http;

class FormHealthRecords extends StatefulWidget {
  State<StatefulWidget> createState() {
    return _FormHealthRecordsState();
  }
}

class _FormHealthRecordsState extends State<FormHealthRecords> {
  String? _hari;
  String? _tanggal;
  String? _suhu;
  String? _saturasi;
  String? _detak;
  String? _sistolik;
  String? _diastolik;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Widget _buildName() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Hari ke-:', hintText: "Masukkan hari"),
      maxLength: 20,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Hari is Required';
        }

        return null;
      },
      onSaved: (String? value) {
        _hari = value!;
      },
    );
  }

  Widget _buildTanggal() {
    return TextFormField(
      decoration:
          InputDecoration(labelText: 'Tanggal:', hintText: "Masukkan tanggal"),
      maxLength: 200,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Message is Required';
        }

        return null;
      },
      onSaved: (String? value) {
        _tanggal = value!;
      },
    );
  }

    Widget _buildSuhu() {
    return TextFormField(
      decoration:
          InputDecoration(labelText: 'Suhu:', hintText: "Masukkan suhu"),
      maxLength: 200,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Message is Required';
        }

        return null;
      },
      onSaved: (String? value) {
        _suhu = value!;
      },
    );
  }

    Widget _buildSaturasi() {
    return TextFormField(
      decoration:
          InputDecoration(labelText: 'Saturasi:', hintText: "Masukkan saturasi"),
      maxLength: 200,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Saturasi is Required';
        }

        return null;
      },
      onSaved: (String? value) {
        _saturasi = value!;
      },
    );
  }

    Widget _buildDetak() {
    return TextFormField(
      decoration:
          InputDecoration(labelText: 'Detak Jantung:', hintText: "Masukkan detak jantung"),
      maxLength: 200,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Detak Jantung is Required';
        }

        return null;
      },
      onSaved: (String? value) {
        _detak = value!;
      },
    );
  }

    Widget _buildSistolik() {
    return TextFormField(
      decoration:
          InputDecoration(labelText: 'Sistolik:', hintText: "Masukkan sistolik"),
      maxLength: 200,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Sistolik is Required';
        }

        return null;
      },
      onSaved: (String? value) {
        _sistolik = value!;
      },
    );
  }

    Widget _buildDiastolik() {
    return TextFormField(
      decoration:
          InputDecoration(labelText: 'Diastolik:', hintText: "Masukkan diastolik"),
      maxLength: 200,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Diastolik is Required';
        }

        return null;
      },
      onSaved: (String? value) {
        _diastolik = value!;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(24),
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            _buildName(),
            _buildTanggal(),
             _buildSuhu(),
             _buildSaturasi(),
             _buildDetak(),
             _buildDiastolik(),
             _buildSistolik(),

            SizedBox(height: 60),
            MaterialButton(
              color: Color.fromRGBO(6, 12, 35, 1),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(14.0)),
                  side: BorderSide(color: Color.fromRGBO(23, 194, 236, 1))),
              onPressed: () async {
                if (!_formKey.currentState!.validate()) {
                  return;
                }

                _formKey.currentState!.save();
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('Feedback Berhasil dikirim!')),
                );
                print("Data");
                print("Hari ke-: " + _hari!);
                print("Tanggal: " + _tanggal!);
                print("Suhu: " + _suhu!);
                print("Saturasi: " + _saturasi!);
                print("Detak Jantung: " + _detak!);
                print("Sistolik: " + _sistolik!);
                print("Diastolik: " + _diastolik!);

                final response = await http.post(Uri.parse("http://10.0.2.2:8000/fetch_feedback"),
                headers: <String, String>{
                  'Content-Type': 'application/json;charset=UTF-8'},
                body: jsonEncode(<String, String>{
                  'Hari ke-': _hari!,
                  'Tanggal': _tanggal!,
                  "Suhu" : _suhu!,
                  "Saturasi" : _saturasi!,
                  "Detak Jantung" : _detak!,
                  "Sistolik" : _sistolik!,
                  "Diastolik" : _diastolik!,
                })
                );
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 14.0),
                child: Text(
                  "Tambahkan",
                  style: TextStyle(color: Colors.white, fontSize: 14.0),
                ),
              ),
            ),
            SizedBox(height: 20),
            MaterialButton(
              color: Color.fromRGBO(6, 12, 35, 1),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(14.0)),
                  side: BorderSide(color: Color.fromRGBO(23, 194, 236, 1))),
              onPressed: () {
                Navigator.of(context).pushNamed(HealthRecordsPage.routeName);
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 14.0),
                child: Text(
                  "Lihat Catatan Kesehatan",
                  style: TextStyle(color: Colors.white, fontSize: 14.0),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            )
          ],
        ),
      ),
    );
  }
}
