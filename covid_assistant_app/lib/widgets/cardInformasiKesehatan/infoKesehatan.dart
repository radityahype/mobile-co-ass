//import 'package:covid_assistant_app/pages/DailyJournalPage/DailyJournalPage.dart';
import 'package:flutter/material.dart';

class InfoKesehatan {
  final int position;
  final String info;
  final String imageUrl;
  final String rendah;
  final String normal;
  final String tinggi;


  InfoKesehatan(this.position, {required this.info, required this.imageUrl, required this.rendah, required this.normal, required this.tinggi,});
}

List<InfoKesehatan> kesehatan = [
  InfoKesehatan(1, info: "Suhu", imageUrl: "assets/images/suhu.png", rendah: "< 35°C", normal: "35-40°C", tinggi: ">40°C"),
  InfoKesehatan(2,info: "Detak Jantung", imageUrl: "assets/images/detak.png", rendah: "≤50mg/dl", normal: "50-70mg/dl", tinggi: ">70mg/dl"),
  InfoKesehatan(3, info: "Saturasi", imageUrl: "assets/images/saturasi.png", rendah: "<80mmHg", normal: "80–100 mmHg", tinggi: ">120mmHg"),
  InfoKesehatan(4, info: "Sistolik", imageUrl: "assets/images/sistolik.png", rendah: "≤89mg/dl", normal: "71-140mg/dl", tinggi: "≥200mg/dl"),
  InfoKesehatan(5, info: "Diastolik", imageUrl: "assets/images/diastolik.png", rendah: "≤60mg/dl", normal: "60-80mg/dl", tinggi: "≥90mg/dl"),
];
