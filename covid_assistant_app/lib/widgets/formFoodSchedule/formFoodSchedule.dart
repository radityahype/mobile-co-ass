import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:covid_assistant_app/pages/FoodSchedulePage/FoodSchedulePage.dart';

import 'package:http/http.dart' as http;

class FormFoodSchedule extends StatefulWidget {
  State<StatefulWidget> createState() {
    return FormFoodScheduleState();
  }
}

class FormFoodScheduleState extends State<FormFoodSchedule> {
  String? tanggal;
  String? waktu;
  String? makan;
  String? minum;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Widget _buildTanggal() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Tanggal', hintText: "Masukkan tanggal Makan dan Minum"),
      maxLength: 20,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Tanggal is Required';
        }

        return null;
      },
      onSaved: (String? value) {
        tanggal = value!;
      },
    );
  }

  Widget _buildWaktu() {
    return TextFormField(
      decoration:
          InputDecoration(labelText: 'Waktu', hintText: "Masukkan Waktu Makan dan Minum anda"),
      maxLength: 20,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Waktu is Required';
        }

        return null;
      },
      onSaved: (String? value) {
        waktu = value!;
      },
    );
  }

  Widget _buildMakan() {
    return TextFormField(
      decoration:
          InputDecoration(labelText: 'Makan', hintText: "Masukkan Apa yang Anda Makan disini"),
      maxLength: 20,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Makan is Required';
        }

        return null;
      },
      onSaved: (String? value) {
        makan = value!;
      },
    );
  }

  Widget _buildMinum() {
    return TextFormField(
      decoration:
          InputDecoration(labelText: 'Minum', hintText: "Masukkan Apa yang Anda Minum disini"),
      maxLength: 20,
      validator: (String? value) {
        if (value!.isEmpty) {
          return 'Minum is Required';
        }

        return null;
      },
      onSaved: (String? value) {
        minum = value!;
      },
    );
  }


  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(24),
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            _buildTanggal(),
            _buildWaktu(),
            _buildMakan(),
            _buildMinum(),
            SizedBox(height: 60),
            MaterialButton(
              color: Color.fromRGBO(6, 12, 35, 1),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(14.0)),
                  side: BorderSide(color: Color.fromRGBO(23, 194, 236, 1))),
              onPressed: () async {
                if (!_formKey.currentState!.validate()) {
                  return;
                }

                _formKey.currentState!.save();
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('Berhasil dikirim!')),
                );
                print("Data");
                print("Tanggal: " + tanggal!);
                print("Waktu: " + waktu!);
                print("Makan: " + makan!);
                print("Minum: " + minum!);
                final response = await http.post(Uri.parse("http://127.0.0.1:8000/fetch_feedback"),
                headers: <String, String>{
                  'Content-Type': 'application/json;charset=UTF-8'},
                body: jsonEncode(<String, String>{
                  'tanggal': tanggal!,
                  'waktu': waktu!,
                  'makan': makan!,
                  'minum': minum!,
                })
                );
                print(response);
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 14.0),
                child: Text(
                  "Tambahkan",
                  style: TextStyle(color: Colors.white, fontSize: 14.0),
                ),
              ),
            ),
            SizedBox(height: 20),
            MaterialButton(
              color: Color.fromRGBO(6, 12, 35, 1),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(14.0)),
                  side: BorderSide(color: Color.fromRGBO(23, 194, 236, 1))),
              onPressed: () {
                Navigator.of(context).pushNamed(FoodSchedulePage.routeName);
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 14.0),
                child: Text(
                  "Lihat Apa yang Sudah Di Makan dan Minum",
                  style: TextStyle(color: Colors.white, fontSize: 14.0),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            )
            // onPressed: () {
            //   if (!_formKey.currentState!.validate()) {
            //     return;
            //   }

            //   _formKey.currentState!.save();

            //   print(_name);
            //   print(_message);

            //   //Send to API
            // },
          ],
        ),
      ),
    );
  }
}
